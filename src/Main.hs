module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)
import Text.Show.Pretty (pPrint)
import qualified Data.Text.IO as Text
import qualified Text.Megaparsec as P

import Data.Functor.Annotate (stripAnn)
import Language.JSE.Decl.Syntax.Parser (parse)
import Language.JSE.Decl.Syntax.Printer (render)

main :: IO ()
main = do
  args <- getArgs
  case args of
    fp : opts -> do
      input <- Text.readFile fp

      case parse fp input of
        Left err -> do
          putStrLn $ P.parseErrorPretty' input err
          exitFailure
        Right parsed -> do
          let stripped = map stripAnn parsed

          if "--render" `elem` opts then
            Text.putStrLn (render stripped)
          else
            if "--strip" `elem` opts then
              pPrint stripped
            else
              pPrint parsed
    _ ->
      putStrLn "JSE parser. Run with a file path argument."
