{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Data.Functor.Annotate
  ( Ann(..)
  , AnnF
  , Ann'
  , annotate
  , Annotated(..)
  , pattern Annotation
  , pattern Annotated
  , composeAnn
  ) where

import Data.Functor.Compose (Compose(..))
import Data.Functor.Foldable (Fix(..), cata)
import Text.Show.Deriving (deriveShow1, deriveShow2)

data Ann ann a = Ann
  { annotation :: ann
  , annotated  :: a
  } deriving (Functor, Show)

type AnnF ann f = Compose (Ann ann) f
type Ann' ann f = Fix (AnnF ann f)

pattern Annotation :: ann -> Fix (AnnF ann a)
pattern Annotation ann <- Fix (Compose (Ann ann _))

pattern Annotated :: g (Ann' ann g) -> Ann' ann g
pattern Annotated a <- Fix (Compose (Ann _ a))

annotate :: ann -> g (Fix (AnnF ann g)) -> Fix (AnnF ann g)
annotate ann = Fix . Compose . Ann ann

class Annotated a where
  type family Stripped a
  stripAnn :: a -> Stripped a

instance Functor f => Annotated (Fix (AnnF ann f)) where
  type Stripped (Fix (AnnF ann f)) = Fix f

  stripAnn :: Functor f => Fix (AnnF ann f) -> Fix f
  stripAnn = cata (Fix . annotated . getCompose)

composeAnn
  :: Functor f
  => f (Ann ann (g (Ann' ann g)))
  -> f (Fix (AnnF ann g))
composeAnn = fmap (Fix . Compose)

$(deriveShow1 ''Ann)
$(deriveShow2 ''Ann)
