{-# LANGUAGE OverloadedStrings #-}

module Language.JSE.Common.Syntax.Lexer
  ( dropWS
  , lexeme
  , symbol
  , operator
  , dot
  , doubleQuote
  , stringLiteral
  , integer
  , reserved
  , qualifiedName
  , name
  , parens
  ) where

import Data.Text (Text)
import Data.List.NonEmpty (NonEmpty)
import qualified Control.Monad.Combinators.NonEmpty as NE
import qualified Data.Text as Text
import qualified Text.Megaparsec as P
import qualified Text.Megaparsec.Char as P
import qualified Text.Megaparsec.Char.Lexer as L

import Language.JSE.Common.Syntax.Parser.Types (Parser)

dropWS :: Parser ()
dropWS = L.space P.space1 lineComment blockComment
  where
    lineComment  = L.skipLineComment "//"
    blockComment = L.skipBlockComment "/*" "*/"

-- | Whitespace will be consumed after every lexeme automatically, but not before it.
lexeme :: Parser a -> Parser a
lexeme = L.lexeme dropWS

symbol :: Char -> Parser Text
symbol c = L.symbol dropWS (Text.singleton c)

operator :: Text -> Parser Text
operator "/" = lexeme . P.try $ P.string "/" <* P.notFollowedBy (P.char '/')
operator op = L.symbol dropWS op

stringLiteral :: Parser Text
stringLiteral = lexeme $ do
  doubleQuote
  fmap Text.pack $ P.manyTill L.charLiteral doubleQuote

dot :: Parser ()
dot = P.char '.' >> pure ()

doubleQuote :: Parser ()
doubleQuote = P.char '"' >> pure ()

integer :: Parser Int
integer = lexeme L.decimal

reserved :: Text -> Parser ()
reserved word = lexeme . P.try $
  P.string word >> P.notFollowedBy P.alphaNumChar

qualifiedName :: Parser (NonEmpty Text)
qualifiedName = lexeme $ name `NE.sepBy1` dot

name :: Parser Text
name = Text.pack <$> P.some P.alphaNumChar

parens :: Parser a -> Parser a
parens = P.between (symbol '(') (symbol ')')
