{-# LANGUAGE PatternSynonyms #-}

module Language.JSE.Common.Syntax.Parser.Annotated
  ( annotateLocation
  , annotateLocation1
  ) where

import qualified Text.Megaparsec as P

import Data.Functor.Annotate
  ( Ann(..)
  , Ann'
  , composeAnn
  )
import Language.JSE.Common.Syntax.Parser.Types
  ( Parser
  , SrcSpan(..)
  )

annotateLocation :: Parser a -> Parser (Ann SrcSpan a)
annotateLocation parser = do
  begin  <- P.getPosition
  result <- parser
  end    <- P.getPosition
  pure $ Ann (SrcSpan begin end) result

annotateLocation1 :: Parser (g (Ann' SrcSpan g)) -> Parser (Ann' SrcSpan g)
annotateLocation1 = composeAnn . annotateLocation
