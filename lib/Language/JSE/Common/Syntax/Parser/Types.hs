{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}

module Language.JSE.Common.Syntax.Parser.Types
  ( Parser
  , ParseError
  , JSEError
  , Loc
  , SrcSpan(..)
  , NamePath
  , ClosedIn
  ) where

import Data.Functor.Compose (Compose(..))
import Data.Functor.Foldable (Fix(..))
import Data.List.NonEmpty (NonEmpty)
import Data.Semigroup (Semigroup(..))
import Data.Text (Text)
import Data.Void (Void)
import Text.Megaparsec (Parsec, SourcePos)
import qualified Text.Megaparsec as P

import Data.Functor.Annotate (Ann(..), Annotated(..))

type Parser = Parsec JSEError Text

type ParseError = P.ParseError (P.Token Text) JSEError

type JSEError = Void

type Loc a = Ann SrcSpan a

data SrcSpan = SrcSpan !SourcePos !SourcePos
  deriving (Eq, Show)

instance Semigroup SrcSpan where
  SrcSpan left _ <> SrcSpan _ right = SrcSpan left right

type NamePath = NonEmpty Text

instance Annotated (Loc NamePath) where
  type Stripped (Loc NamePath) = NamePath
  stripAnn (Ann _ np) = np

type family ClosedIn b r where
  ClosedIn b (Fix (Compose (Ann ann) r)) = Fix (Compose (Ann ann) b)
  ClosedIn b r                           = Fix b
