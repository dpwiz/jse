module Language.JSE.Common.Syntax.Printer
  ( render
  ) where

import Data.Text (Text)
import Data.Text.Prettyprint.Doc (Doc, layoutCompact)
import Data.Text.Prettyprint.Doc.Render.Text (renderStrict)

render :: (a -> Doc ann) -> a -> Text
render printer = renderStrict
  . layoutCompact
  . printer
