{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Language.JSE.Decl.Syntax.Printer
  ( render
  , printer
  ) where

import Data.Text (Text)
import Data.Text.Prettyprint.Doc (Doc, (<+>))
import Data.Void (Void)
import qualified Data.Text.Prettyprint.Doc as PP

import Language.JSE.Decl.Syntax.Types (DeclE(..), Decl)
import qualified Language.JSE.Common.Syntax.Printer as Common
import qualified Language.JSE.JSExpr.Syntax.Printer as JSExpr
import qualified Language.JSE.JSType.Syntax.Printer as JSType

render :: PP.Pretty (DeclE jst jse) => [DeclE jst jse] -> Text
render = Common.render printer

printer :: PP.Pretty (DeclE jst jse) => [DeclE jst jse] -> Doc Void
printer = PP.vcat . map PP.pretty

instance PP.Pretty Decl where
  pretty = \case
    Let name jse ->
      PP.group $
        "let" <+> PP.pretty name <+> "=" <+> JSExpr.printer jse
    Type name jst ->
      PP.group $
        "type" <+> PP.pretty name <+> "=" <+> JSType.printer jst
