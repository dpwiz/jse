{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Language.JSE.Decl.Syntax.Types
  ( LocalName
  , DeclE(..)
  , Decl
  , DeclLoc
  ) where

import Data.Text (Text)

import Data.Functor.Annotate (Ann(..), Annotated(..))
import qualified Language.JSE.Common.Syntax.Parser.Types as Common
import qualified Language.JSE.JSExpr.Syntax.Types as JSExpr
import qualified Language.JSE.JSType.Syntax.Types as JSType

type LocalName = Text

data DeclE jsexpr jstype
  = Let LocalName jsexpr
  | Type LocalName jstype
  deriving (Eq, Show)

type Decl = DeclE
  JSExpr.JSExpr
  JSType.JSType

type DeclLoc = Common.Loc
  (DeclE
    JSExpr.JSExprLoc
    JSType.JSTypeLoc
  )

instance Annotated DeclLoc where
  type Stripped DeclLoc = Decl

  stripAnn (Ann _ dl) =
    case dl of
      Let ln expr ->
        Let ln (stripAnn expr)
      Type ln typ ->
        Type ln (stripAnn typ)
