{-# LANGUAGE OverloadedStrings #-}

module Language.JSE.Decl.Syntax.Parser
  ( parse
  , topLevel
  , declarations
  , declaration
  , module Language.JSE.Decl.Syntax.Types
  ) where

import Data.Text (Text)
import Text.Megaparsec ((<?>))
import qualified Text.Megaparsec as P

import Language.JSE.Common.Syntax.Parser.Annotated (annotateLocation)
import Language.JSE.Common.Syntax.Parser.Types (Parser, ParseError)
import Language.JSE.Decl.Syntax.Types (DeclE(..), DeclLoc)

import qualified Language.JSE.Common.Syntax.Lexer as L
import qualified Language.JSE.JSExpr.Syntax.Parser as JSExpr
import qualified Language.JSE.JSType.Syntax.Parser as JSType

parse :: FilePath -> Text -> Either ParseError [DeclLoc]
parse filePath input = P.parse topLevel filePath input

topLevel :: Parser [DeclLoc]
topLevel = P.between L.dropWS P.eof (P.some declaration)

declarations :: Parser [DeclLoc]
declarations = P.some declaration

declaration :: Parser DeclLoc
declaration = P.choice
  [ typeBinding <?> "type binding"
  , letBinding <?> "let binding"
  ]

typeBinding :: Parser DeclLoc
typeBinding = annotateLocation $ do
  L.reserved "type"
  name <- L.lexeme L.name
  _ <- L.symbol '='
  value <- JSType.jsType
  pure (Type name value)

letBinding :: Parser DeclLoc
letBinding = annotateLocation $ do
  L.reserved "let"
  name <- L.lexeme L.name
  _ <- L.symbol '='
  value <- JSExpr.expr
  pure (Let name value)
