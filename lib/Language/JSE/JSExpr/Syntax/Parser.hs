{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Language.JSE.JSExpr.Syntax.Parser
  ( parse
  , parseTest
  , topLevel
  , expr
  , module Language.JSE.JSExpr.Syntax.Types
  ) where

import Prelude hiding (null)

import Data.Functor (($>))
import Data.Semigroup ((<>))
import Data.Text (Text)
import Text.Megaparsec ((<?>))
import qualified Text.Megaparsec as P
import qualified Text.Megaparsec.Expr as P

import Data.Functor.Annotate
  ( Ann(..)
  , pattern Annotation
  , annotate
  , stripAnn
  )
import Language.JSE.Common.Syntax.Parser.Annotated
  ( annotateLocation
  , annotateLocation1
  )
import Language.JSE.JSExpr.Syntax.Types
  ( JSExprF(..)
  , JSExpr
  , JSExprLoc
  , UnaryOp(..)
  , BinaryOp(..)
  )
import Language.JSE.Common.Syntax.Parser.Types
  ( Parser
  , ParseError
  , SrcSpan(..)
  )
import qualified Language.JSE.Common.Syntax.Lexer as L

parse :: FilePath -> Text -> Either ParseError JSExprLoc
parse filePath input = P.parse topLevel filePath input

parseTest :: Text -> IO JSExpr
parseTest input =
  case parse "<input>" input of
    Left err -> do
      putStrLn $ P.parseErrorPretty' input err
      fail "error"
    Right exprLoc ->
      pure $ stripAnn exprLoc

topLevel :: Parser JSExprLoc
topLevel = P.between L.dropWS P.eof expr <?> "top-level value"

expr :: Parser JSExprLoc
expr = P.makeExprParser term ops
  where
    term :: Parser JSExprLoc
    term = P.choice
      [ L.parens expr
      , null
      , litBool
      , litInt
      , litString
      , litPath
      , array
      , dict
      ]

    ops =
      [ [ prefix "-" NumNegate        {- 1 -}
        ]
      , [ binaryR "++" ArrConcat      {- 2 -}
        , binaryR "+>" DictMergeRight {- 3 -}
        , binaryR "<+" DictMergeLeft  {- 4 -}
        ]
      , [ binaryL "*" NumMultiply     {- 5 -}
        , binaryL "/" NumDivide       {- 5 -}
        ]
      , [ binaryL "+" NumAdd          {- 6 -}
        , binaryL "-" NumSubtract     {- 6 -}
        ]
      , [ prefix "!" BoolNot          {- 7 -}
        ]
      , [ binaryL "&&" BoolAnd        {- 8 -}
        ]
      , [ binaryL "||" BoolOr         {- 9 -}
        ]
      , [ binaryN "==" PredEqual      {- 10 -}
        , binaryN "!=" PredNotEqual   {- 11 -}
        ]
      ]

    prefix  name op = P.Prefix (manyUnaryOp $ opWithLoc name op unary)
    binaryL name op = P.InfixL (opWithLoc name op binary)
    binaryN name op = P.InfixN (opWithLoc name op binary)
    binaryR name op = P.InfixR (opWithLoc name op binary)

    manyUnaryOp f = foldr1 (.) <$> P.some f

    opWithLoc :: Text -> op -> (Ann SrcSpan op -> a) -> Parser a
    opWithLoc name op f = do
      Ann ann _ <- annotateLocation $ L.operator name
      pure . f $ Ann ann op

    unary :: Ann SrcSpan UnaryOp -> JSExprLoc -> JSExprLoc
    unary (Ann opSpan op) exprloc = annotate span' expr'
      where
        span' = opSpan <> exprSpan
        expr' = UnaryF op exprloc
        Annotation exprSpan = exprloc

    binary :: Ann SrcSpan BinaryOp -> JSExprLoc -> JSExprLoc -> JSExprLoc
    binary (Ann opSpan op) leftloc rightloc = annotate span' expr'
      where
        span' = leftSpan <> opSpan <> rightSpan
        expr' = BinaryF op leftloc rightloc
        Annotation leftSpan = leftloc
        Annotation rightSpan = rightloc

null :: Parser JSExprLoc
null = annotateLocation1 $
  L.lexeme $ L.reserved "null" $> NullF

litBool :: Parser JSExprLoc
litBool = annotateLocation1
  ( P.choice
    [ L.lexeme $ L.reserved "true"  $> LitBoolF True
    , L.lexeme $ L.reserved "false" $> LitBoolF False
    ]
  ) <?> "boolean literal"

litInt :: Parser JSExprLoc
litInt = annotateLocation1
  ( LitIntF <$> L.integer
  ) <?> "integer literal"

litString :: Parser JSExprLoc
litString = annotateLocation1
  ( LitStringF <$> L.stringLiteral
  ) <?> "string literal"

array :: Parser JSExprLoc
array = annotateLocation1 $ do
  xs <- P.between (L.symbol '[') (L.symbol ']') $
    expr `P.sepBy` L.symbol ','
  pure (ArrayF xs)

dict :: Parser JSExprLoc
dict = annotateLocation1 $ do
  kvs <- P.between (L.symbol '{') (L.symbol '}') $
    pair `P.sepBy` L.symbol ','
  pure (DictF kvs)
  where
    pair = do
      key <- L.stringLiteral <?> "dictionary key"
      _ <- L.symbol ':'
      value <- expr
      pure (key, value)

litPath :: Parser JSExprLoc
litPath = annotateLocation1
  ( LitPathF <$> L.qualifiedName <?> "identifier"
  )
