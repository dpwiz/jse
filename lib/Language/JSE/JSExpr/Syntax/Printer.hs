{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Language.JSE.JSExpr.Syntax.Printer
  ( render
  , printer
  , JSExprPrettyIn
  , PP.pretty
  ) where

import Data.Foldable (toList)
import Data.Functor.Foldable (cata)
import Data.Semigroup ((<>))
import Data.Text (Text)
import Data.Text.Prettyprint.Doc (Doc, (<+>))
import Data.Text.Prettyprint.Doc.Internal.Type (Doc(Annotated))
import qualified Data.Text as Text
import qualified Data.Text.Prettyprint.Doc as PP

import Data.Functor.Annotate (stripAnn)
import qualified Language.JSE.Common.Syntax.Printer as Common
import Language.JSE.JSExpr.Syntax.Types
  ( JSExpr
  , JSExprLoc
  , JSExprIn
  , JSExprF(..)
  , UnaryOp(..)
  , BinaryOp(..)
  )

render :: JSExpr -> Text
render = Common.render printer

printer :: JSExpr -> Doc ann
printer = PP.pretty

instance PP.Pretty JSExpr where
  pretty = PP.unAnnotate . cata docAlg

instance PP.Pretty JSExprLoc where
  pretty = PP.pretty . stripAnn

-- * Unpretty internals

docAlg :: JSExprF (Doc Precedence) -> Doc Precedence
docAlg = \case
  NullF          -> PP.annotate Term $ "null"
  LitBoolF True  -> PP.annotate Term $ "true"
  LitBoolF False -> PP.annotate Term $ "false"
  LitIntF int    -> PP.annotate Term $ PP.pretty int
  LitStringF str -> PP.annotate Term $ prettyString str
  LitPathF path  -> PP.annotate Term $ prettyPath path
  ArrayF xs      -> PP.annotate Term $ prettyList xs
  DictF kvs      -> PP.annotate Term $ prettyDict kvs
  UnaryF op expr -> prettyUnary op expr
  BinaryF op x y -> prettyBinary op x y

  where
    prettyString = PP.dquotes
      . PP.pretty
      . Text.replace "\n" "\\n"
      . Text.replace "\"" "\\\""
      . Text.replace "\\" "\\\\"

    prettyList = PP.brackets . mconcat . PP.punctuate ","

    prettyDict pairs = PP.braces . mconcat $ PP.punctuate ","
      [ PP.dquotes (PP.pretty key) <> (":" <+> value)
      | (key, value) <- pairs
      ]

    prettyPath = PP.pretty . Text.intercalate "." . toList

    prettyUnary op expr = PP.annotate prec $
      PP.group (op' <> addParens prec expr)
      where
        (op', prec) = case op of
          NumNegate -> ("-", Pref 1)
          BoolNot   -> ("!", Pref 7)

    prettyBinary op x y = PP.annotate prec $
      PP.group (addParens prec x <+> op' <+> addParens prec y)
      where
        (op', prec) = case op of
          NumAdd         -> ("+",  InfL  6)
          NumSubtract    -> ("-",  InfL  6)
          NumMultiply    -> ("*",  InfL  5)
          NumDivide      -> ("/",  InfL  5)
          BoolAnd        -> ("&&", InfL  8)
          BoolOr         -> ("||", InfL  9)
          ArrConcat      -> ("++", InfR  2)
          DictMergeRight -> ("+>", InfR  3)
          DictMergeLeft  -> ("<+", InfR  4)
          PredEqual      -> ("==", InfN 10)
          PredNotEqual   -> ("!=", InfN 11)

    addParens :: Precedence -> Doc Precedence -> Doc Precedence
    addParens op sub
      | dontWrap  = sub
      | optional  = sub
      | otherwise = PP.parens sub
      where
        dontWrap =
          precedence subPrec < precedence op

        optional =
          precedence subPrec == precedence op
            && op /= Term
            && sameAssoc subPrec op

        subPrec = case sub of
          Annotated prec _ -> prec
          _                -> Term -- XXX: recurse?

        precedence = \case
          Term   -> minBound
          InfN p -> p
          InfL p -> p
          InfR p -> p
          Pref p -> p

        sameAssoc a b = case (a, b) of
          (Term,   Term)   -> True
          (InfN _, InfN _) -> True
          (InfL _, InfL _) -> True
          (InfR _, InfR _) -> True
          (Pref _, Pref _) -> True
          _                -> False

data Precedence
  = Term
  | InfN Int
  | InfL Int
  | InfR Int
  | Pref Int
  deriving (Eq)

type JSExprPrettyIn r = PP.Pretty (JSExprIn r)
