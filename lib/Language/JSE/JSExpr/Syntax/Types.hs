{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Language.JSE.JSExpr.Syntax.Types
  ( JSExpr
  , pattern Null
  , pattern LitBool
  , pattern LitInt
  , pattern LitString
  , pattern LitPath
  , pattern Array
  , pattern Dict
  , pattern Unary
  , pattern Binary
  , UnaryOp(..)
  , BinaryOp(..)
  , JSExprF(..)
  , JSExprLoc
  , JSExprIn
  , pattern Name
  ) where

import Data.Eq.Deriving (deriveEq1)
import Data.Functor.Foldable (Fix(..))
import Data.List.NonEmpty (NonEmpty(..))
import Data.Text (Text)
import Text.Show.Deriving (deriveShow1)

import Data.Functor.Annotate (AnnF)
import qualified Language.JSE.Common.Syntax.Parser.Types as Common

pattern Name :: Text -> JSExpr
pattern Name name = LitPath (name :| [])

data JSExprF r
  = NullF
  | LitBoolF   Bool
  | LitIntF    Int
  | LitStringF Text
  | LitPathF   Common.NamePath
  | ArrayF     [r]
  | DictF      [(Text, r)]
  | UnaryF     UnaryOp r
  | BinaryF    BinaryOp r r
  deriving (Eq, Functor, Foldable, Traversable, Show)

data UnaryOp
  = NumNegate
  | BoolNot
  deriving (Eq, Enum, Bounded, Show)

data BinaryOp
  = NumAdd
  | NumSubtract
  | NumMultiply
  | NumDivide
  | BoolAnd
  | BoolOr
  | ArrConcat
  | DictMergeRight
  | DictMergeLeft
  | PredEqual
  | PredNotEqual
  deriving (Eq, Enum, Bounded, Show)

type JSExpr = Fix JSExprF

pattern Null :: JSExpr
pattern Null = Fix NullF

pattern LitBool :: Bool -> JSExpr
pattern LitBool val = Fix (LitBoolF val)

pattern LitInt :: Int -> JSExpr
pattern LitInt val = Fix (LitIntF val)

pattern LitString :: Text -> JSExpr
pattern LitString val = Fix (LitStringF val)

pattern LitPath :: Common.NamePath -> JSExpr
pattern LitPath names = Fix (LitPathF names)

pattern Array :: [JSExpr] -> JSExpr
pattern Array vals = Fix (ArrayF vals)

pattern Dict :: [(Text, JSExpr)] -> JSExpr
pattern Dict vals = Fix (DictF vals)

pattern Unary :: UnaryOp -> JSExpr -> JSExpr
pattern Unary op expr = Fix (UnaryF op expr)

pattern Binary :: BinaryOp -> JSExpr -> JSExpr -> JSExpr
pattern Binary op left right = Fix (BinaryF op left right)

type JSExprLocF = AnnF Common.SrcSpan JSExprF

type JSExprLoc = Fix JSExprLocF

type JSExprIn r = Common.ClosedIn JSExprF r

$(deriveEq1 ''JSExprF)
$(deriveShow1 ''JSExprF)
