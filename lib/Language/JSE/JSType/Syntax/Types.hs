{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}

module Language.JSE.JSType.Syntax.Types
  ( JSTypeE(..)
  , JSType
  , JSTypeLoc
  , JSTypeCtxE(..)
  , JSTypeCtx
  , JSTypeCtxLoc
  , ConstraintE(..)
  , Constraint
  , ConstraintLoc
  ) where

import Data.List.NonEmpty (NonEmpty)
import Data.Semigroup (Semigroup(..))
import Data.Text (Text)

import Data.Functor.Annotate (Ann(..), Annotated(..))
import qualified Language.JSE.Common.Syntax.Parser.Types as Common
import qualified Language.JSE.JSExpr.Syntax.Types as JSExpr

-- * Type declaration

-- (a : Foo, b : Bar || Baz) => {"a": a, "b": [b | Int]}
data JSTypeE tctx texpr = JSTypeE tctx texpr
  deriving (Eq, Show)

type JSType = JSTypeE JSTypeCtx JSExpr.JSExpr

type JSTypeLoc = Common.Loc
  (JSTypeE
    JSTypeCtxLoc
    JSExpr.JSExprLoc
  )

instance Annotated JSTypeLoc where
  type Stripped JSTypeLoc = JSType

  stripAnn (Ann _ (JSTypeE tctx texpr)) =
    JSTypeE (stripAnn tctx) (stripAnn texpr)

-- * Type context

-- a : Foo, b : Bar || Baz
newtype JSTypeCtxE cons = JSTypeCtxE [cons]
  deriving (Eq, Show)

instance Semigroup (JSTypeCtxE cons) where
  JSTypeCtxE left <> JSTypeCtxE right =
    JSTypeCtxE (left <> right)

instance Monoid (JSTypeCtxE cons) where
  mempty = JSTypeCtxE mempty
  mappend = (<>)

type JSTypeCtx = JSTypeCtxE Constraint

type JSTypeCtxLoc = Common.Loc (JSTypeCtxE ConstraintLoc)

instance Annotated JSTypeCtxLoc where
  type Stripped JSTypeCtxLoc = JSTypeCtx

  stripAnn (Ann _ (JSTypeCtxE cs)) =
    JSTypeCtxE (map stripAnn cs)

-- a : Foo || bar || baz
data ConstraintE cons = ConstraintE Text (NonEmpty cons)
  deriving (Eq, Show)

type Constraint = ConstraintE Common.NamePath

type ConstraintLoc = Common.Loc
  (ConstraintE
    (Common.Loc Common.NamePath)
  )

instance Annotated ConstraintLoc where
  type Stripped ConstraintLoc = Constraint

  stripAnn (Ann _ (ConstraintE ln cs)) =
    ConstraintE ln (fmap annotated cs)
