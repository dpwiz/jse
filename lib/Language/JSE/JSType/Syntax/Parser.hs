{-# LANGUAGE OverloadedStrings #-}

module Language.JSE.JSType.Syntax.Parser
  ( jsType
  , jsTypeCtx
  ) where

import qualified Control.Monad.Combinators.NonEmpty as NE
import qualified Text.Megaparsec as P

import Language.JSE.Common.Syntax.Parser.Types (Parser)
import Language.JSE.Common.Syntax.Parser.Annotated (annotateLocation)
import Language.JSE.JSType.Syntax.Types
  ( JSTypeE(..)
  , JSTypeLoc
  , JSTypeCtxE(..)
  , JSTypeCtxLoc
  , ConstraintE(..)
  , ConstraintLoc
  )
import qualified Language.JSE.Common.Syntax.Lexer as L
import qualified Language.JSE.JSExpr.Syntax.Parser as JSExpr

-- Foo
-- x : spam => x
-- (x : spam, y : sau.sa.ge) => x
jsType :: Parser JSTypeLoc
jsType = P.choice
  [ constrained
  , naked
  ]
  where
    constrained = annotateLocation $ do
      tctx <- jsTypeCtx <* L.operator "=>"
      texpr <- JSExpr.expr
      pure (JSTypeE tctx texpr)

    naked = annotateLocation $ do
      tctx <- annotateLocation (pure mempty)
      texpr <- JSExpr.expr
      pure (JSTypeE tctx texpr)

jsTypeCtx :: Parser JSTypeCtxLoc
jsTypeCtx = annotateLocation (JSTypeCtxE <$> constraints)
  where
    constraints = P.choice
      [ L.parens (constraint `P.sepBy` L.symbol ',')
      , pure <$> constraint
      ]

constraint :: Parser ConstraintLoc
constraint = annotateLocation . P.try $ do
  name <- L.lexeme L.name <* L.symbol ':'
  cons <- annotateLocation L.qualifiedName `NE.sepBy1` L.operator "||"
  pure (ConstraintE name cons)
