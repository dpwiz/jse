{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Language.JSE.JSType.Syntax.Printer
  ( render
  , printer
  , PP.pretty
  ) where

import Data.Foldable (toList)
import Data.List.NonEmpty(NonEmpty(..))
import Data.Text (Text)
import Data.Text.Prettyprint.Doc (Doc, (<+>))
import qualified Data.Text as Text
import qualified Data.Text.Prettyprint.Doc as PP

import Language.JSE.JSType.Syntax.Types
  ( JSTypeE(..)
  , JSType
  , JSTypeCtxE(..)
  , ConstraintE(..)
  )
import qualified Language.JSE.Common.Syntax.Printer as Common
import qualified Language.JSE.JSExpr.Syntax.Printer as JSExpr

render
  :: PP.Pretty (JSTypeE tctx texpr)
  => JSTypeE tctx texpr -> Text
render = Common.render PP.pretty

printer
  :: PP.Pretty (JSTypeE tctx texpr)
  => JSTypeE tctx texpr -> Doc ann
printer = PP.pretty

instance PP.Pretty JSType where
  pretty (JSTypeE tctx texpr) =
    PP.group $
      case tctx of
        JSTypeCtxE [] ->
          JSExpr.pretty texpr
        JSTypeCtxE [single] ->
          PP.hsep $
            [ prettyConstraint single
            , "=>"
            , JSExpr.pretty texpr
            ]
        JSTypeCtxE multiple ->
          PP.hsep
            [ PP.parens . PP.hsep . PP.punctuate "," $
                map prettyConstraint multiple
            , "=>"
            , JSExpr.pretty texpr
            ]
    where
      prettyConstraint (ConstraintE name as) =
        PP.pretty name <+> ":" <+> prettyAlts as

      prettyAlts (single :| []) =
        prettyPath single
      prettyAlts multiple =
        PP.hsep $ PP.punctuate " ||"
          [ prettyPath namePath
          | namePath <- toList multiple
          ]

      prettyPath = PP.pretty . Text.intercalate "." . toList
