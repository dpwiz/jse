{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Main where

import Test.Tasty (TestTree, defaultMain, testGroup)

import qualified Language.JSE.Decl.Syntax.Tests as Decl
import qualified Language.JSE.JSExpr.Syntax.Tests as JSExpr

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "JSE"
  [ syntaxTests
  ]

syntaxTests :: TestTree
syntaxTests = testGroup "Syntax"
  [ JSExpr.syntaxTests
  , Decl.syntaxTests
  ]
