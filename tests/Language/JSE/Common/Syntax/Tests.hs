{-# LANGUAGE TypeFamilies #-}

module Language.JSE.Common.Syntax.Tests
  ( bicodec
  , parseIn
  , parse
  , parse_
  , parseIn1
  , parse1
  , parse1_
  , parseError
  , parseError_
  ) where

import Control.Error (flipEither)
import Data.Text (Text)
import Hedgehog ((===))
import Text.Megaparsec (parseErrorPretty')
import qualified Hedgehog as Hedgehog

import Data.Functor.Annotate (Annotated(..))
import Language.JSE.Common.Syntax.Parser.Types

type Parsing a = FilePath -> Text -> Either ParseError a

-- * Parser wrappers

-- | Check both parsed ast and rendered text for exact values.
--
-- XXX: Hedgehog.tripping checks only functions.
bicodec
  :: (Eq ast, Show ast, Hedgehog.MonadTest m)
  => (Text -> m ast)
  -> (ast -> Text)
  -> Text
  -> ast
  -> m ()
bicodec parser render text ast = do
  parser text >>= \res -> ast === res
  render ast === text

parseIn
  :: (Annotated a, Show a, Monad m, s ~ Stripped a)
  => Parsing a -> (s -> m s) -> Text -> m s
parseIn parsing monad input =
  case parsing "<tests>" input of
    Right result ->
      monad (stripAnn result)
    Left err ->
      fail (parseErrorPretty' input err)

parse
  :: (Annotated a, Show a, Hedgehog.MonadTest m)
  => Parsing a -> Text -> m (Stripped a)
parse parsing = parseIn parsing pure

parse_
  :: (Annotated a, Show a, Hedgehog.MonadTest m)
  => Parsing a -> Text -> m ()
parse_ parsing input = parse parsing input >> pure ()

-- ** Lifted

parseIn1
  :: ( Functor f
     , Annotated a
     , Show a
     , Monad m
     , b ~ Stripped a
     )
  => Parsing (f a) -> (f b -> m (f b)) -> Text -> m (f b)
parseIn1 parsing monad input =
  case parsing "<tests>" input of
    Right result ->
      monad (fmap stripAnn result)
    Left err ->
      fail (parseErrorPretty' input err)

parse1
  :: (Functor f, Annotated a, Show a, Hedgehog.MonadTest m)
  => Parsing (f a) -> Text -> m (f (Stripped a))
parse1 parsing = parseIn1 parsing pure

parse1_
  :: (Functor f, Annotated a, Show a, Hedgehog.MonadTest m)
  => Parsing (f a) -> Text -> m ()
parse1_ parsing input = parse1 parsing input >> pure ()

-- ** Errors

parseError
  :: (Show a, Hedgehog.MonadTest m)
  => Parsing a -> Text -> m ParseError
parseError parsing input = do
  Hedgehog.footnote "Parser error expected, but did not happen."
  Hedgehog.evalEither . flipEither $ parsing "<tests>" input

parseError_
  :: (Show a, Hedgehog.MonadTest m)
  => Parsing a -> Text -> m ()
parseError_ parsing input = parseError parsing input >> pure ()
