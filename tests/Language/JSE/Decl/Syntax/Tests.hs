{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}

module Language.JSE.Decl.Syntax.Tests
  ( syntaxTests
  ) where

import Data.Semigroup ((<>))
import Data.Text (Text)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)
import qualified Hedgehog as Hedgehog

import Language.JSE.JSExpr.Syntax.Printer () -- Instances
import Language.JSE.Decl.Syntax.Types (Decl, DeclE(..))
import qualified Language.JSE.Common.Syntax.Tests as Common
import qualified Language.JSE.Decl.Syntax.Parser as Decl
import qualified Language.JSE.Decl.Syntax.Printer as Decl
import qualified Language.JSE.JSExpr.Syntax.Types as JSExpr
import qualified Language.JSE.JSType.Syntax.Types as JSType

syntaxTests :: TestTree
syntaxTests = testGroup "Decl"
  [ testProperty "Empty" checkParserEmpty
  , testProperty "Trailing garbage" checkParserTrailingGarbage
  , testGroup "Type"
    [ testProperty "Empty context" checkTypeNoCtx
    , testProperty "Simple context" checkTypeSimpleCtx
    , testProperty "Complex context" checkTypeComplexCtx
    ]
  , testProperty "Let" checkLet
  , testProperty "Multiple" checkMultiple
  ]

-- * Parser

checkParserEmpty :: Hedgehog.Property
checkParserEmpty = Hedgehog.property $
  parseError_ mempty

checkParserTrailingGarbage :: Hedgehog.Property
checkParserTrailingGarbage = Hedgehog.property $
  parseError_ "let foo = bar asdasd"

-- * Types

checkTypeNoCtx :: Hedgehog.Property
checkTypeNoCtx = Hedgehog.property $ bicodec text decls
  where
    text =
      "type Foo = Int"

    decls =
      [ Type "Foo" (JSType.JSTypeE mempty jsType)
      ]

    jsType = JSExpr.Name "Int"

checkTypeSimpleCtx :: Hedgehog.Property
checkTypeSimpleCtx = Hedgehog.property $ bicodec text decls
  where
    text =
      "type Foo = foo : any => foo"

    decls =
      [ Type "Foo" (JSType.JSTypeE ctx jsType)
      ]

    ctx = JSType.JSTypeCtxE
      [ JSType.ConstraintE
          "foo"
          (pure $ pure "any")
      ]

    jsType = JSExpr.Name "foo"

checkTypeComplexCtx :: Hedgehog.Property
checkTypeComplexCtx = Hedgehog.property $ bicodec text decls
  where
    text =
      "type Foo = (foo : my.Null, bar : Int || Bool) => foo || String"

    decls =
      [ Type "Foo" (JSType.JSTypeE ctx jsType)
      ]

    ctx = JSType.JSTypeCtxE
      [ JSType.ConstraintE
          "foo"
          (pure $ pure "my" <> pure "Null") -- segmented name
      , JSType.ConstraintE
          "bar"
          (pure (pure "Int") <> pure (pure "Bool")) -- multiple alternatives
      ]

    jsType = JSExpr.Binary
      JSExpr.BoolOr -- XXX: not really a bool
      (JSExpr.Name "foo")
      (JSExpr.Name "String")

-- * Lets

checkLet :: Hedgehog.Property
checkLet = Hedgehog.property $ bicodec text decls
  where
    text =
      "let foo = null"

    decls =
      [ Let "foo" JSExpr.Null
      ]

-- ** Multiple declarations

checkMultiple :: Hedgehog.Property
checkMultiple = Hedgehog.property $ bicodec text decls
  where
    text =
      "let foo = bar\nlet bar = baz"

    decls =
      [ Let "foo" (JSExpr.Name "bar")
      , Let "bar" (JSExpr.Name "baz")
      ]

-- * Decl-specific wrappers

parse :: Hedgehog.MonadTest m => Text -> m [Decl]
parse = Common.parse1 Decl.parse

parseError_ :: Hedgehog.MonadTest m => Text -> m ()
parseError_ = Common.parseError_ Decl.parse

render :: [Decl] -> Text
render = Decl.render

bicodec :: Hedgehog.MonadTest m => Text -> [Decl] -> m ()
bicodec = Common.bicodec parse render
