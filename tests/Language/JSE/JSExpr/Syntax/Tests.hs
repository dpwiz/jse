{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Language.JSE.JSExpr.Syntax.Tests
  ( syntaxTests
  ) where

import Data.Functor.Identity (Identity(..))
import Data.List.NonEmpty (NonEmpty(..))
import Data.Text (Text)
import Hedgehog (forAll, (===))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)
import qualified Hedgehog as Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import qualified Data.Text as Text

import Language.JSE.JSExpr.Syntax.Types
  ( JSExpr
  , pattern Null
  , pattern LitBool
  , pattern LitInt
  , pattern LitString
  , pattern LitPath
  , pattern Array
  , pattern Dict
  , pattern Unary
  , pattern Binary
  , UnaryOp(..)
  , BinaryOp(..)
  , pattern Name
  )

import qualified Language.JSE.Common.Syntax.Tests as Common
import qualified Language.JSE.JSExpr.Syntax.Gen as Gen
import qualified Language.JSE.JSExpr.Syntax.Parser as JSExpr
import qualified Language.JSE.JSExpr.Syntax.Printer as JSExpr

syntaxTests :: TestTree
syntaxTests = testGroup "JSExpr"
  [ testGroup "Parser"
    [ testProperty "Empty" checkParserEmpty
    , testProperty "Trailing garbage" checkParserTrailingGarbage
    ]
  , testProperty "Null" checkNull
  , testGroup "Literals"
    [ testProperty "Bool" checkLitBool
    , testProperty "Int" checkLitInt
    , testProperty "String" checkLitString
    , testProperty "String escapes" checkLitStringEsc
    , testProperty "Single-item path" checkPathName
    , testProperty "Path" checkPath
    ]
  , testGroup "Arrays"
    [ testProperty "Empty" checkArrayEmpty
    , testProperty "Singleton" checkArraySingleton
    , testProperty "Flat" checkArrayFlat
    , testProperty "Nested" checkArrayNested
    ]
  , testGroup "Dictionaries"
    [ testProperty "Empty" checkDictEmpty
    , testProperty "Singleton" checkDictSingleton
    , testProperty "Flat" checkDictFlat
    , testProperty "Nested" checkDictNested
    ]
  , testGroup "Expressions"
    [ testProperty "Unary" checkExprUnary
    , testProperty "Binary" checkExprBinary
    , testProperty "Mixed" checkExprMixed
    ]
  , testProperty "Parentheses" checkParens
  , testProperty "Term trees" checkTreeTerm
  , testProperty "Expression trees" checkTreeExpr
  ]

-- * Parser basics

checkParserEmpty :: Hedgehog.Property
checkParserEmpty = Hedgehog.property $
  parseError_ mempty

checkParserTrailingGarbage :: Hedgehog.Property
checkParserTrailingGarbage = Hedgehog.property $
  parseError_ "true sddsfdsf"

-- * Literals

checkNull :: Hedgehog.Property
checkNull = Hedgehog.property $
  parse "null" >>= \Null ->
    pure ()

checkLitBool :: Hedgehog.Property
checkLitBool = Hedgehog.property $ do
  bicodec "true" (LitBool True)
  bicodec "false" (LitBool False)

checkLitInt :: Hedgehog.Property
checkLitInt = Hedgehog.property $ do
  int <- forAll Gen.enumBounded
  bicodec (Text.pack $ show int) (LitInt int)

checkLitString :: Hedgehog.Property
checkLitString = Hedgehog.property $ do
  unquoted <- forAll
    . fmap Text.pack
    . Gen.list (Range.exponential 0 1000)
    $ Gen.filter (\c -> c `notElem` ['\\', '"']) Gen.unicode
  let quoted = mconcat ["\"", unquoted, "\""]
  bicodec quoted (LitString unquoted)

checkLitStringEsc :: Hedgehog.Property
checkLitStringEsc = Hedgehog.property $ do
  bicodec "\"new\\nline\""      (LitString "new\nline")
  bicodec "\"dash\\\\ing\""     (LitString "dash\\ing")
  bicodec "\"double\\\"quote\"" (LitString "double\"quote")

-- ** Paths

checkPathName :: Hedgehog.Property
checkPathName = Hedgehog.property $ do
  bicodec "x" (Name "x")
  bicodec "xAbc123" (Name "xAbc123")
  bicodec "Abc123" (Name "Abc123")
  bicodec "юникод" (Name "юникод")

  parseError_ "lol🐈" -- unexpected '🐈'

checkPath :: Hedgehog.Property
checkPath = Hedgehog.property $ do
  bicodec "x.y.z" . LitPath $ "x" :| ["y", "z"]
  bicodec "X.Y.Z" . LitPath $ "X" :| ["Y", "Z"]

  -- Reserved words are syntactically ok in the tail
  bicodec "is.true" . LitPath $ "is" :| ["true"]

  -- Even number-only parts are acceptable
  bicodec "ip.127.0.0.1" . LitPath $ "ip" :| ["127", "0", "0", "1"]

  bicodec "да.конечно" . LitPath $ "да" :| ["конечно"]

-- * Arrays

checkArrayEmpty :: Hedgehog.Property
checkArrayEmpty = Hedgehog.property $
  bicodec "[]" (Array [])

checkArraySingleton :: Hedgehog.Property
checkArraySingleton = Hedgehog.property $
  bicodec "[null]" (Array [Null])

checkArrayFlat :: Hedgehog.Property
checkArrayFlat = Hedgehog.property $ bicodec text expr
  where
    text =
      "[null,true,42,\"foo\"]"
    
    expr = Array
      [ Null
      , LitBool True
      , LitInt 42
      , LitString "foo"
      ]

checkArrayNested :: Hedgehog.Property
checkArrayNested = Hedgehog.property $ bicodec text expr
  where
    text =
      "[true,[],[42]]"

    expr = Array
      [ LitBool True
      , Array []
      , Array [LitInt 42]
      ]

-- * Dicts (name/expr assoc arrays)

checkDictEmpty :: Hedgehog.Property
checkDictEmpty = Hedgehog.property $
  bicodec "{}" (Dict [])

checkDictSingleton :: Hedgehog.Property
checkDictSingleton = Hedgehog.property $
  bicodec "{\"key\": null}" (Dict [("key", Null)])

checkDictFlat :: Hedgehog.Property
checkDictFlat = Hedgehog.property $ bicodec text expr
  where
    text = "{\"n\": null,\"b\": true,\"i\": 42,\"s\": \"foo\"}"

    expr = Dict
      [ ("n", Null)
      , ("b", LitBool True)
      , ("i", LitInt 42)
      , ("s", LitString "foo")
      ]

checkDictNested :: Hedgehog.Property
checkDictNested = Hedgehog.property $ bicodec text expr
  where
    text =
      "{\"o\": {\"k\": 42}}"
    
    expr = Dict
      [ ( "o"
        , Dict
          [ ("k", LitInt 42)
          ]
        )
      ]

-- * Expressions

checkExprUnary :: Hedgehog.Property
checkExprUnary = Hedgehog.property $ do
  bicodec "!true" $ Unary BoolNot (LitBool True)

  bicodec "-0" $ Unary NumNegate (LitInt 0)

  -- Yes, this would be a type error
  bicodec "!0" $ Unary BoolNot (LitInt 0)

  -- Whitespace-insensitive parsing
  parse "! true" >>= \(Unary op term) -> do
    op   === BoolNot
    term === LitBool True

checkExprBinary :: Hedgehog.Property
checkExprBinary = Hedgehog.property $ do
  bicodec "42 / 0" $ Binary NumDivide (LitInt 42) (LitInt 0)

  -- Comments do not cause propblems with similarly-looking ops
  parse "42/0 // ha!" >>= \(Binary op left right) -> do
    op    === NumDivide
    left  === LitInt 42
    right === LitInt 0

checkExprMixed :: Hedgehog.Property
checkExprMixed = Hedgehog.property $ do
  let negOne = Unary NumNegate (LitInt 1)
  bicodec "-1 - -1" $ Binary NumSubtract negOne negOne

  let left  = Binary NumAdd (LitInt 42) (Unary NumNegate (LitInt 42))
      right = Unary NumNegate (LitInt 0)
  bicodec "42 + -42 == -0" $ Binary PredEqual left right

-- * Parentheses

checkParens :: Hedgehog.Property
checkParens = Hedgehog.property $ bicodec text expr
  where
    text = "foo && (1 + 2 != 0) || bar"

    expr = Binary BoolOr left (Name "bar")

    left =
      Binary BoolAnd (Name "foo") $
        Binary PredNotEqual onePlusTwo (LitInt 0)

    onePlusTwo = Binary NumAdd (LitInt 1) (LitInt 2)

-- * Arbitrary trees

checkTreeTerm :: Hedgehog.Property
checkTreeTerm = Hedgehog.property $ do
  term <- forAll Gen.term
  Hedgehog.tripping term render parsePure

checkTreeExpr :: Hedgehog.Property
checkTreeExpr = Hedgehog.property $ do
  expr <- forAll Gen.expr
  -- XXX: this deals with non-normalized expressions
  -- BUG: but allows for subtle printer bugs
  let rendered = render expr
  Hedgehog.tripping rendered parsePure (fmap render)

-- * Expr-specific wrappers

parsePure :: Text -> Identity JSExpr
parsePure = Common.parseIn JSExpr.parse Identity

parse :: Hedgehog.MonadTest m => Text -> m JSExpr
parse = Common.parse JSExpr.parse

parseError_ :: Hedgehog.MonadTest m => Text -> m ()
parseError_ = Common.parseError_ JSExpr.parse

render :: JSExpr.JSExpr -> Text
render = JSExpr.render

bicodec :: Hedgehog.MonadTest m => Text -> JSExpr -> m ()
bicodec = Common.bicodec parse render
