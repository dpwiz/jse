{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

-- | AST generators

module Language.JSE.JSExpr.Syntax.Gen
  ( term
  , expr
  , null
  , bool
  , int
  , string
  , path
  , array
  , dict
  ) where

import Prelude hiding (null)

import Data.List.NonEmpty (NonEmpty(..))
import qualified Hedgehog as Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Language.JSE.JSExpr.Syntax.Types
  ( JSExpr
  , pattern Null
  , pattern LitBool
  , pattern LitInt
  , pattern LitString
  , pattern LitPath
  , pattern Array
  , pattern Dict
  , pattern Unary
  , pattern Binary
  , UnaryOp(..)
  , BinaryOp(..)
  )

term :: Hedgehog.MonadGen m => m JSExpr
term = Gen.recursive Gen.choice nonRecusive
  [ array term
  , dict term
  ]

expr :: Hedgehog.MonadGen m => m JSExpr
expr = Gen.recursive Gen.choice nonRecusive
  [ array expr
  , dict expr
  , boolUnary
  , boolBinary
  , numUnary
  , numBinary
  ]

nonRecusive :: Hedgehog.MonadGen m => [m JSExpr]
nonRecusive =
  [ null
  , bool
  , int
  , string
  , path
  ]

null :: Hedgehog.MonadGen m => m JSExpr
null = Gen.constant Null

bool :: Hedgehog.MonadGen m => m JSExpr
bool = Gen.element [LitBool True, LitBool False]

int :: Hedgehog.MonadGen m => m JSExpr
int = LitInt <$> Gen.enumBounded

string :: Hedgehog.MonadGen m => m JSExpr
string = LitString <$> Gen.text (Range.exponential 0 512) Gen.unicode

path :: Hedgehog.MonadGen m => m JSExpr
path = LitPath <$> genPathNames
  where
    genPathNames = (:|)
      <$> genPathHead
      <*> Gen.list (Range.exponential 1 32) genPathName
    genPathHead = Gen.filter reserved $ Gen.text (Range.exponential 1 64) Gen.alpha
    genPathName = Gen.text (Range.exponential 1 64) Gen.alphaNum
    reserved str = str `notElem` ["null", "true", "false"]

array :: Hedgehog.MonadGen m => m JSExpr -> m JSExpr
array rec = Array <$> Gen.list (Range.exponential 0 128) rec

dict :: Hedgehog.MonadGen m => m JSExpr -> m JSExpr
dict rec = Dict <$> Gen.list (Range.exponential 0 128) genPair
  where
    genPair = (,) <$> genKey <*> rec
    genKey = Gen.text (Range.exponential 0 128) Gen.unicode

boolUnary :: Hedgehog.MonadGen m => m JSExpr
boolUnary = Unary
  <$> pure BoolNot
  <*> Gen.choice [ bool, path ] -- TODO: genBoolArg

boolBinary :: Hedgehog.MonadGen m => m JSExpr
boolBinary = Binary
  <$> Gen.element
    [ BoolAnd
    , BoolOr
    ]
  <*> genBoolArg
  <*> genBoolArg

genBoolArg :: Hedgehog.MonadGen m => m JSExpr
genBoolArg = Gen.recursive Gen.choice nonrec rec
  where
    nonrec = [ bool, path ]
    rec = [ boolUnary, boolBinary ]

numUnary :: Hedgehog.MonadGen m => m JSExpr
numUnary = Unary
  <$> pure NumNegate
  <*> Gen.choice [ int, path ] -- TODO: numArg

numBinary :: Hedgehog.MonadGen m => m JSExpr
numBinary = Binary
  <$> Gen.element
    [ NumAdd
    , NumSubtract
    , NumMultiply
    , NumDivide
    ]
  <*> numArg
  <*> numArg

numArg :: Hedgehog.MonadGen m => m JSExpr
numArg = Gen.recursive Gen.choice nonrec rec
  where
    nonrec = [ int, path ]
    rec = [ numUnary, numBinary ]
