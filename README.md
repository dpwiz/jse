# JSON with expressions

Starring...
  * recursion-schemes
  * megaparsec
  * hedgehog
  * prettyprinting

## Example

test.jse:

```json
type Fnord = Int

type FooBar =
  ( foo : Int || Bool || Fnord
  , bar : JSON.any
  ) =>
  { "foo": foo
  , "bar": bar || JSON.null
  }

let test =
  [ "Hello"
  , this.is.a.test
  , { "yes": [ This.JSON, has, path.literals ]
    , "also": "comments" // but of course!
    }
  , expressions == terms && operators
  ]
```

Parsed (location stripped):

```haskell
[ Type
    "Fnord" (JSTypeE (JSTypeCtxE []) (Fix (LitPathF ("Int" :| []))))
, Type
    "FooBar"
    (JSTypeE
       (JSTypeCtxE
          [ ConstraintE
              "foo" (("Int" :| []) :| [ "Bool" :| [] , "Fnord" :| [] ])
          , ConstraintE "bar" (("JSON" :| [ "any" ]) :| [])
          ])
       (Fix
          (DictF
             [ ( "foo" , Fix (LitPathF ("foo" :| [])) )
             , ( "bar"
               , Fix
                   (BinaryF
                      BoolOr
                      (Fix (LitPathF ("bar" :| [])))
                      (Fix (LitPathF ("JSON" :| [ "null" ]))))
               )
             ])))
, Let
    "test"
    (Fix
       (ArrayF
          [ Fix (LitStringF "Hello")
          , Fix (LitPathF ("this" :| [ "is" , "a" , "test" ]))
          , Fix
              (DictF
                 [ ( "yes"
                   , Fix
                       (ArrayF
                          [ Fix (LitPathF ("This" :| [ "JSON" ]))
                          , Fix (LitPathF ("has" :| []))
                          , Fix (LitPathF ("path" :| [ "literals" ]))
                          ])
                   )
                 , ( "also" , Fix (LitStringF "comments") )
                 ])
          , Fix
              (BinaryF
                 PredEqual
                 (Fix (LitPathF ("expressions" :| [])))
                 (Fix
                    (BinaryF
                       BoolAnd
                       (Fix (LitPathF ("terms" :| [])))
                       (Fix (LitPathF ("operators" :| []))))))
          ]))
]
```

## Reasonable errors

```json
[ "Hello" lol
]
```

```
  |
1 | [ "Hello" lol
  |           ^
unexpected 'l'
expecting ',', ']', or operator
```
